# Control de heladas


This project implements a IoT system for frost control on farms.

Node sensor. It is in charge of measuring environmental variables. The device communicates with the temperature, relative humidity and pressure sensor via I2C and with the soil moisture sensor by means of an adjusted input. It is wirelessly connected to the LoRaWAN network through the Gateway that uses RF LoRa modulation. It works with battery and is located in a geographical point of the fruit cultivation.

Gateway. It is in charge of receiving the data transmitted by the sensor nodes and forwarding them to the LoRaWAN network server and vice versa. It communicates with the sensor nodes through RF Lora and with the server via Wi-Fi.

