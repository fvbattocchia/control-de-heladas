"""
inicializo LORAWAN, conecto a TTN por ABP, clase de dispositivo LoRaWAN A
creo socket para trasmitir a TTN,
envio datos a TTN.
si no envia el mensaje a TTN muestra mensaje de error con el led en rojo.
se implemeta deep sleep por 30 min
se implementa lora nvram methods para guardar los datos de la conexion
"""
from network import LoRa
import binascii
import socket
import config
import pycom
import time
import struct
import machine

class LoraConnection:
    def __init__(self):
        # initialize LoRa in LORAWAN mode.
        print("init LoRa")
        self.lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.US915)
        time.sleep(1) #Allows us to exit the code using ctrl+c

        # create an ABP authentication params
        self.dev_addr = struct.unpack(">l", binascii.unhexlify(config.dev_address))[0]
        self.nwk_swkey = binascii.unhexlify(config.nwk_key)
        self.app_swkey = binascii.unhexlify(config.app_key)

        # remove all the channels
        for channel in range(0, 72):
            self.lora.remove_channel(channel)

        # set all channels to the same frequency (must be before sending the OTAA join request)
        for channel in range(0, 72):
            self.lora.add_channel(channel, frequency=config.LORA_FREQUENCY, dr_min=0, dr_max=3)

        self.lora.nvram_restore()

    def connectToTTN(self):
        if(self.lora.has_joined() == False):
            print("LoRa not joined yet")
            # join a network using ABP
            self.lora.join(activation=LoRa.ABP, auth=(self.dev_addr, self.nwk_swkey, self.app_swkey))
        else:
            print("LoRa Joined")
        self._createSocket()

    def _createSocket(self):
        # create a LoRa socket
        self.s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
        # set the LoRaWAN data rate
        self.s.setsockopt(socket.SOL_LORA, socket.SO_DR, config.LORA_NODE_DR)

    def sendData(self,buffer):
        print('Send data...')
        self.s.setblocking(True)
        try:
            self.s.send(bytes(buffer))
        except Exception as ex:
            print("Error: {0}".format(ex))
            pycom.rgbled(config.red)
            time.sleep(2)
            pycom.rgbled(config.off)
        self.s.setblocking(False)
        rx, port = self.s.recvfrom(256)
        if rx:
            print('Received: {}, on port: {}'.format(rx, port))
        self.lora.nvram_save()
        print("sleeping")
        machine.deepsleep(config.sleep_time)
