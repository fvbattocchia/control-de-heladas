""" LoPy LoRaWAN Nano Gateway configuration options """

# for US915
LORA_FREQUENCY = 903900000
LORA_NODE_DR = 3
#LORA_GW_DR = "SF7BW125"

# Colors
off = 0x000000
red = 0xff0000
green = 0x00ff00
blue = 0x0000ff

dev_address = '260C6378'
nwk_key = 'F79E62E3C4775348D62B9FEDA85112C2'
app_key = '55B761A812124A7A9F219C444ED7E9C1'

sleep_time = 10000 # sleep for 10 seconds
#sleep_time = 1000*60 * 30 # sleep for 30 min
#sleep_time = 1000*60  # sleep for 1 min
