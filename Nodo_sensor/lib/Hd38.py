#!/usr/bin/env python
"""
sensor de humedad del suelo
4095-----0% aire
800 ----- 100% agua
"""
import machine
class Hd38:
    numADCreadings = const(100)

    def __init__(self):
        self.in_min = 800
        self.in_max = 4095
        self.out_min = 0
        self.out_max = 100
        adc = machine.ADC()
        adc.init()
        self.adcchannel = adc.channel(pin='P13',attn=adc.ATTN_11DB)

    def readHumidityFloor(self):
        meanADC = 0.0
        i = 0
        while (i < numADCreadings):
            value = self.adcchannel.value()
            meanADC += value
            i += 1
        meanADC /= numADCreadings   #promedio
        if meanADC < self.in_min:
            meanADC = self.in_min
        return 100 - self._map(meanADC)

    def _map(self,x):
        return int((x-self.in_min) * (self.out_max-self.out_min) / (self.in_max-self.in_min) + self.out_min)
