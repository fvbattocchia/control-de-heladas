"""Cayenne Low Power Payload (LPP) proporciona una forma cómoda y sencilla 
de enviar datos a través de redes LPWAN como LoRaWAN. El Cayenne LPP cumple 
con la restricción de tamaño de carga útil, que se puede reducir a 11 bytes y
permite que el dispositivo envíe múltiples datos de sensores a la vez."""

import struct
import math

LPP_DIGITAL_INPUT = 0         # 1 byte
LPP_DIGITAL_OUTPUT = 1        # 1 byte
LPP_ANALOG_INPUT = 2          # 2 bytes, 0.01 signed
LPP_ANALOG_OUTPUT = 3         # 2 bytes, 0.01 signed
LPP_TEMPERATURE = 103         # 2 bytes, 0.1°C signed
LPP_RELATIVE_HUMIDITY = 104   # 1 byte, 0.5% unsigned
LPP_BAROMETRIC_PRESSURE = 115 # 2 bytes 0.1 hPa Unsigned


# Data ID + Data Type + Data Size
LPP_DIGITAL_INPUT_SIZE = 3       # 1 byte
LPP_DIGITAL_OUTPUT_SIZE = 3      # 1 byte
LPP_ANALOG_INPUT_SIZE = 4        # 2 bytes, 0.01 signed
LPP_ANALOG_OUTPUT_SIZE = 4       # 2 bytes, 0.01 signed
LPP_TEMPERATURE_SIZE = 4         # 2 bytes, 0.1°C signed
LPP_RELATIVE_HUMIDITY_SIZE = 3   # 1 byte, 0.5% unsigned
LPP_BAROMETRIC_PRESSURE_SIZE = 4 # 2 bytes 0.1 hPa Unsigned

class CayenneLPP:
    def __init__(self):
        self.buffer = bytearray()

    def get_buffer(self):
        return self.buffer

    def reset(self):
        self.buffer = bytearray()

    def get_size(self):
        return len(self.buffer)

    def add_temperature(self, channel, value):
        val = math.floor(value * 10)

        self.buffer.extend(struct.pack('b', channel))
        self.buffer.extend(struct.pack('b', LPP_TEMPERATURE))
        self.buffer.extend(struct.pack('b', val >> 8))
        self.buffer.extend(struct.pack('b', val))

    def add_relative_humidity(self, channel, value):
        val = math.floor(value * 2)

        self.buffer.extend(struct.pack('b', channel))
        self.buffer.extend(struct.pack('b', LPP_RELATIVE_HUMIDITY))
        self.buffer.extend(struct.pack('b', val))

    def add_digital_input(self, channel, value):
        self.buffer.extend(struct.pack('b', channel))
        self.buffer.extend(struct.pack('b', LPP_DIGITAL_INPUT))
        self.buffer.extend(struct.pack('b', value))

    def add_digital_output(self, channel, value):
        self.buffer.extend(struct.pack('b', channel))
        self.buffer.extend(struct.pack('b', LPP_DIGITAL_OUTPUT))
        self.buffer.extend(struct.pack('b', value))

    def add_analog_input(self, channel, value):
        val = math.floor(value * 100)

        self.buffer.extend(struct.pack('b', channel))
        self.buffer.extend(struct.pack('b', LPP_ANALOG_INPUT))
        self.buffer.extend(struct.pack('b', val >> 8))
        self.buffer.extend(struct.pack('b', val))

    def add_analog_output(self, channel, value):
        val = math.floor(value * 100)

        self.buffer.extend(struct.pack('b', channel))
        self.buffer.extend(struct.pack('b', LPP_ANALOG_OUTPUT))
        self.buffer.extend(struct.pack('b', val >> 8))
        self.buffer.extend(struct.pack('b', val))

    def add_barometric_pressure(self, channel, value):
        val = math.floor(value * 10)

        self.buffer.extend(struct.pack('b', channel))
        self.buffer.extend(struct.pack('b', LPP_BAROMETRIC_PRESSURE))
        self.buffer.extend(struct.pack('b', val >> 8))
        self.buffer.extend(struct.pack('b', val))                
