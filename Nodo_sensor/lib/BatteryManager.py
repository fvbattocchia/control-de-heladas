#!/usr/bin/env python
import machine
"""
Vout = pin16
Vin = Vbat
R = R2/(R1+R2)
1) Vout= R Vin

resolucion ADC =(2*12)-1 = 4095
Att = Vmax o Vref - rango aceptable de voltage del adc

vref ---- resolucion adc
Vout ----  lectura adc

2) Vout =    vref* lectura
            ---------------
     	      resolucion

            vref * lectura
 R * Vin = ---------------
             resolucion

         vref * lectura
Vin =   ---------------
         resolucion * R
"""

class BatteryManager:
    numADCreadings = const(100)

    def __init__(self):
        self.vref=3.3   #vreferencia dado por la atenuacion 11db 1/0.3
        self.res=4095
        self.R=0.5     #factor divisor de tension r1=r2=100
        adc = machine.ADC()
        adc.init()
        self.adcchannel = adc.channel(pin='P16',attn=adc.ATTN_11DB)

    def readBatteryVoltage(self):
        meanADC = 0.0
        i = 0
        while (i < numADCreadings):
            value = self.adcchannel.value()
            meanADC += value
            i += 1
        meanADC /= numADCreadings   #promedio
        return ((meanADC * self.vref) / ( self.res * self.R ))
