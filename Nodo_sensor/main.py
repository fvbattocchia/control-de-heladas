#!/usr/bin/env python

""" ABP Node example compatible with the LoPy Nano Gateway """

from LoraConnection import LoraConnection
from CayenneLPP import CayenneLPP
from BatteryManager import BatteryManager
from Hd38 import Hd38
from BME280 import BME280
from machine import I2C
import config
import time

pycom.rgbled(config.green)
# BME280 default address.
BME280_I2CADDR_1 = 0x76
BME280_I2CADDR_2 = 0x77

i2c = I2C(0, I2C.MASTER, baudrate=400000)#100000

bme1 = BME280(address=BME280_I2CADDR_1, i2c=i2c)
bme2 = BME280(address=BME280_I2CADDR_2, i2c=i2c)
hd38 = Hd38()
batteryMngr = BatteryManager()

lora = LoraConnection()
lora.connectToTTN()
lpp = CayenneLPP()

bme1.temperature
bme2.temperature
bme2.pressure
bme1.humidity

lpp.reset()
lpp.add_temperature(1,bme1.temperature) #temperatura sensor 1 [*C]
lpp.add_temperature(2,bme2.temperature) #temperatura sensor 2 [*C]
lpp.add_relative_humidity(3,bme1.humidity) #humedad relativa [%]
lpp.add_relative_humidity(4,hd38.readHumidityFloor()) #humedad del suelo relativa [%]
lpp.add_barometric_pressure(5,bme2.pressure) #presion [hPa]
lpp.add_analog_input(6,batteryMngr.readBatteryVoltage()) #voltage de la bateria [V]
print('Send: {}'.format(lpp.get_buffer()))
lora.sendData(lpp.get_buffer())
