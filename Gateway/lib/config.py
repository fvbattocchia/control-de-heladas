""" LoPy LoRaWAN Nano Gateway configuration options """

import machine
import ubinascii

WIFI_MAC = ubinascii.hexlify(machine.unique_id()).upper()
# Set  the Gateway ID to be the first 3 bytes of MAC address + 'FFFE' + last 3 bytes of MAC address
GATEWAY_ID = WIFI_MAC[:6] + "FFFE" + WIFI_MAC[6:12]

#for US915
SERVER = 'nam1.cloud.thethings.network'
PORT = 1700

NTP = "pool.ntp.org"
NTP_PERIOD_S = 3600

WIFI_SSID = 'avc6085341'
WIFI_PASS = 'Jadm18012019'
#WIFI_SSID = 'CTNET-SOBISCH'
#WIFI_PASS = 'gabriel24'
#WIFI_SSID = 'JORGE ORTIZ'
#WIFI_PASS = '16573720'
#WIFI_SSID = 'avc6085341_2.4EXT'
#WIFI_PASS = 'Jadm18012019'

# for US915 US_902_928_FSB_2
# for US915
LORA_FREQUENCY = 903900000
LORA_GW_DR = "SF7BW125" # DR_3
LORA_NODE_DR = 3

#LORA_FREQUENCY = 903900000
#LORA_GW_DR = "SF10BW125" # DR_0
#LORA_NODE_DR = 0

#for US915
#LORA_GW_DR = "SF7BW125" # DR_3 -903.9 - SF7BW125 to SF10BW125
#LORA_NODE_DR = 3

"""
SF Spread factor: Higher means more range and better reception, and more airtime
BW Bandwidth
"""
